FROM ubuntu:14.04

RUN apt-get update -qq && \
    apt-get install -y -qq python-pip supervisor openssh-server && \
    rm -rf /var/lib/apt/lists/*

RUN pip install shadowsocks

COPY supervisord.conf /etc/supervisor/
COPY supervisord_shadowsocks.conf /etc/supervisor/conf.d/
COPY shadowsocks.json /etc/

EXPOSE 22 7777

RUN useradd phoenix && usermod -p '$6$jEFqXjPs$/JRBzj2aen1zMh4pj8sPUci/9PKuNSOivFbIxcxxdoeu6Q.RWhTJQii.H4e7nZORacR1HqSZy9hS2aRSNdBPN0' phoenix
COPY id_rsa.pub /home/phoenix/.ssh/id_rsa.pub

ENTRYPOINT /usr/bin/supervisord